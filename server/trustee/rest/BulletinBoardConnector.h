#ifndef MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H
#define MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H


#include <mk-tfhe-voting/helpers/IBulletinBoardForTrustee.h>
#include <cpprest/http_client.h>

class BulletinBoardConnector : public IBulletinBoardForTrustee {
	web::http::client::http_client client;

	web::http::http_response postRequest(const web::uri &uri, const web::json::value &body);

public:
	explicit BulletinBoardConnector(const web::uri &base_uri);

	void sendPartialResults(std::vector<std::vector<Torus32>> vector) override;
};


#endif //MK_TFHE_VOTING_BULLETINBOARDCONNECTOR_H
