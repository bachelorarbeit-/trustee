#include "BulletinBoardConnector.h"


BulletinBoardConnector::BulletinBoardConnector(const web::uri &base_uri) : client{base_uri} {}

web::http::http_response BulletinBoardConnector::postRequest(const web::uri &uri, const web::json::value &body) {
	web::http::http_request httpRequest{web::http::methods::POST};
	httpRequest.set_request_uri(uri);
	httpRequest.set_body(body);
	auto res = client.request(httpRequest).get();
	if (res.status_code() != web::http::status_codes::OK) std::cout << "not successful " << res.status_code() << std::endl;
	return res;
}

void BulletinBoardConnector::sendPartialResults(std::vector<std::vector<Torus32>> vector) {
	web::json::value body = web::json::value::array();
	for (unsigned counterId{}; counterId < vector.size(); ++counterId) {
		web::json::value counter = web::json::value::array();
		for (unsigned bitId{}; bitId < vector.at(counterId).size(); ++bitId) {
			counter[bitId] = web::json::value::number(vector.at(counterId).at(bitId));
		}
		body[counterId] = counter;
	}
	postRequest("/sendpartialresults", body);
}