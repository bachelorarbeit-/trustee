#include "CoordinatorConnector.h"
#include "BulletinBoardConnector.h"

#include <mk-tfhe-voting/helpers/Params.h>
#include "ParamsFromJson.h"

#include <mk-tfhe-voting/exceptions/CoordinatorError.h>
#include <mk-tfhe-voting/exceptions/JsonError.h>

#include <thread>
#include <chrono>


CoordinatorConnector::CoordinatorConnector(web::uri const &coordinatorUri, web::uri selfUri) : client{coordinatorUri}, self{std::move(selfUri)} {}


web::http::http_response CoordinatorConnector::tryRequest(const web::http::http_request &httpRequest) {
	using namespace std::chrono_literals;
	web::http::http_response httpResponse;

	bool isMaxDelayReached{false};
	unsigned int maxDelay{12};
	unsigned int unsuccessfulResponse{1};
	unsigned int multipleDelay{1};

	while (true) {
		try {
			httpResponse = client.request(httpRequest).get();
			break;
		} catch (web::http::http_exception const &exception) {
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{exception.what()} + " " + client.base_uri().to_string() + httpRequest.request_uri().to_string()));
			if (!isMaxDelayReached) {
				if (unsuccessfulResponse % 12 == 0) {
					++multipleDelay;
					if (multipleDelay > maxDelay) {
						isMaxDelayReached = true;
					}
				}
				++unsuccessfulResponse;
			} else {
				multipleDelay = maxDelay;
			}
			std::this_thread::sleep_for(5s * multipleDelay);
		}
	}
	checkResponse(httpResponse);
	return httpResponse;
}

void CoordinatorConnector::checkResponse(const web::http::http_response &httpResponse) {
	if (httpResponse.status_code() != web::http::status_codes::OK) {
		throw CoordinatorError("Coordinator status code " + std::to_string(httpResponse.status_code()));
	}
}

web::http::http_response CoordinatorConnector::getRequest(const web::uri &uri) {
	web::http::http_request httpRequest{web::http::methods::GET};
	httpRequest.headers().add("Authorization", apiKey);
	httpRequest.set_request_uri(uri);
	return tryRequest(httpRequest);
}

web::http::http_response CoordinatorConnector::postRequest(const web::uri &uri, const web::json::value &body) {
	web::http::http_request httpRequest{web::http::methods::POST};
	httpRequest.headers().add("Authorization", apiKey);
	httpRequest.set_request_uri(uri);
	httpRequest.set_body(body);
	return tryRequest(httpRequest);
}

void CoordinatorConnector::registerTrusteeToCoordinator() {
	web::json::value body = web::json::value::object();
	body["hostname"] = web::json::value::string(self.host());
	body["port"] = web::json::value::number(self.port());

	try {
		auto json = postRequest("/trustee/register", body).extract_json().get();
		apiKey = "Bearer " + json.at("apiKey").as_string();
		trusteeNumber = json.at("id").as_number().to_uint32();
	} catch (web::json::json_exception const &exception) {
		throw JsonError("Parsing register response failed", exception);
	}
}

std::vector<VotesText> CoordinatorConnector::getVotes() {
	try {
		auto json = postRequest("/trustee/getVotes", web::json::value::object()).extract_json().get();
		std::vector<VotesText> votesInfo{};
		for (auto &vote : json.as_array()) {
			VotesText votesText{};
			votesText.question = vote.at("question").as_string();
			votesInfo.push_back(votesText);
		}
		return votesInfo;
	} catch (web::json::json_exception const &exception) {
		throw JsonError("Parsing votes text failed", exception);
	}
}

std::shared_ptr<TrusteeParams> CoordinatorConnector::registerTrusteeToVote(unsigned voteNumber) {
	try {
		auto json = postRequest("/trustee/registerToVote/" + std::to_string(voteNumber), web::json::value::object()).extract_json().get();
		unsigned trusteeId{};
		std::vector<std::shared_ptr<IBulletinBoardForTrustee>> bulletinBoards{};
		std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey{};
		Params params{};
		params = getParamsFromJson(json.at("voteParams"));
		for (auto &trustee : json.at("bulletinBoards").as_array()) {
			web::uri_builder uriBuilder{};
			uriBuilder.set_host(trustee.at("bulletinBoard").at("hostname").as_string());
			uriBuilder.set_port(trustee.at("bulletinBoard").at("port").as_number().to_uint32());
			bulletinBoards.emplace_back(std::make_shared<BulletinBoardConnector>(uriBuilder.to_uri()));
		}

		for (auto &trustee : json.at("trustees").as_array()) {
			if (trustee.at("id").as_number().to_uint32() == this->trusteeNumber) {
				trusteeId = trustee.at("trusteeOrder").as_number().to_uint32();
			}
		}

		for (auto rLwePubKeyJson : json.at("rLwePubKey").as_array()) {
			auto torusPolynomial = std::make_shared<TorusPolynomial>(params.N);
			for (unsigned int i = 0; i < static_cast<unsigned int>(params.N); i++) {
				torusPolynomial->coefsT[i] = rLwePubKeyJson.at("coefsT").as_array().at(i).as_number().to_int32();
			}
			rLwePubKey.push_back(torusPolynomial);
		}
		return std::make_shared<TrusteeParams>(trusteeId, voteNumber, bulletinBoards, rLwePubKey, params);
	} catch (web::json::json_exception const &exception) {
		throw JsonError("Parsing params failed", exception);
	}
}

void CoordinatorConnector::publishKeys(std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> pubKey, int32_t n, unsigned voteId) {
	web::json::value body = web::json::value::array();
	for (std::size_t sampleId{}; sampleId < pubKey->size(); ++sampleId) {
		web::json::value sample = web::json::value::object();
		sample["id"] = web::json::value::number(sampleId);
		sample["phase"] = pubKey->at(sampleId)->b;
		for (int i{}; i < n; ++i) {
			sample["phaseShift"][i] = pubKey->at(sampleId)->a[i];
		}
		body[sampleId] = sample;
	}
	postRequest("/trustee/publishKeys/" + std::to_string(voteId), body);
}

void CoordinatorConnector::publishResults(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<Torus32>> partialDecrypt) {
	web::json::value body{web::json::value::array()};
	for (unsigned counterId{}; counterId < partialDecrypt.size(); ++counterId) {
		web::json::value counter = web::json::value::array();
		for (unsigned bitId{}; bitId < partialDecrypt.at(counterId).size(); ++bitId) {
			counter[bitId] = web::json::value::number(partialDecrypt.at(counterId).at(bitId));
		}
		body[counterId] = counter;
	}
	postRequest("/trustee/results/" + std::to_string(voteId) + "/" + std::to_string(bulletinBoardId), body);
}
