#ifndef MK_TFHE_VOTING_JSONCOVERSION_H
#define MK_TFHE_VOTING_JSONCOVERSION_H


#include <cpprest/http_client.h>
#include <mkTFHEsamples.h>
#include <lwesamples.h>
#include <polynomials.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <memory>

static web::json::value jsonFromPolynomial(TorusPolynomial const &polynomial) {
	web::json::value torusPolynomialJson = web::json::value::object();
	torusPolynomialJson["N"] = web::json::value::number(polynomial.N);
	for (int j{}; j < polynomial.N; ++j) {
		torusPolynomialJson["coefsT"][j] = web::json::value::number(polynomial.coefsT[j]);
	}
	return torusPolynomialJson;
}

static web::json::value jsonFromRlwePub(std::vector<std::shared_ptr<TorusPolynomial>> const &rLwePubKey) {
	web::json::value rLwePubKeyJson = web::json::value::array();
	for (unsigned i{}; i < rLwePubKey.size(); ++i) {
		rLwePubKeyJson[i] = jsonFromPolynomial(*rLwePubKey.at(i));
	}
	return rLwePubKeyJson;
}

static web::json::value jsonFromBootstrappingKey(std::vector<std::shared_ptr<MKTGswUESample>> const &bootstrappingKey) {
	web::json::value bootstrappingKeyJson = web::json::value::array();
	for (unsigned i{}; i < bootstrappingKey.size(); ++i) {
		auto &key{bootstrappingKey.at(i)};
		for (int j{}; j < 6 * key->dg; ++j) {
			bootstrappingKeyJson[i]["c"][j] = jsonFromPolynomial(key->c[j]);
		}
		bootstrappingKeyJson[i]["party"] = web::json::value::number(key->party);
		bootstrappingKeyJson[i]["current_variance"] = web::json::value::number(key->current_variance);
		bootstrappingKeyJson[i]["dg"] = web::json::value::number(key->dg);
		bootstrappingKeyJson[i]["N"] = web::json::value::number(key->N);
	}
	return bootstrappingKeyJson;
}

static web::json::value jsonFromLweSample(LweSample const &sample, int n) {
	web::json::value sampleJson = web::json::value::object();
	sampleJson["phase"] = web::json::value::number(sample.b);
	sampleJson["current_variance"] = web::json::value::number(sample.current_variance);
	for (int i{}; i < n; ++i) {
		sampleJson["phaseShift"][i] = web::json::value::number(sample.a[i]);
	}
	return sampleJson;
}

static web::json::value jsonFromKsk(LweKeySwitchKey const *ks) {
	web::json::value ksJson = web::json::value::object();
	ksJson["n"] = web::json::value::number(ks->n);
	ksJson["t"] = web::json::value::number(ks->t);
	ksJson["basebit"] = web::json::value::number(ks->basebit);
	ksJson["base"] = web::json::value::number(ks->base);
	for (int i{}; i < ks->n * ks->t * ks->base; ++i) {
		ksJson["ks"][i] = jsonFromLweSample(ks->ks0_raw[i], ks->out_params->n);
	}
	return ksJson;
}

static std::pair<unsigned int, std::vector<std::vector<std::shared_ptr<MKLweSample>>>>
countersFromJson(web::json::value const &countersJson, std::shared_ptr<LweParams> const &lweParams, std::shared_ptr<MKTFHEParams> const &mkTfheParams) {
	std::vector<std::vector<std::shared_ptr<MKLweSample>>> counters{};
	unsigned int id = countersJson.at("bbId").as_number().to_uint32();

	for (auto &counterJson : countersJson.at("counters").as_array()) {
		std::vector<std::shared_ptr<MKLweSample>> counter{};
		for (auto &bitJson : counterJson.at("counterBits").as_array()) {
			counter.emplace_back(std::make_shared<MKLweSample>(lweParams.get(), mkTfheParams.get()));
			// TODO n necessary below?
			unsigned n = bitJson.at("phaseShift").as_array().size();
			for (int j{}; j < mkTfheParams->n * mkTfheParams->parties; ++j) {
				counter.back()->a[j] = bitJson.at("phaseShift").at(j).as_number().to_int32();
			}
			counter.back()->b = bitJson.at("phase").as_number().to_int32();
		}
		counters.push_back(counter);
	}
	return std::make_pair(id, counters);
}

#endif //MK_TFHE_VOTING_JSONCONVERSION_H
