#ifndef MK_TFHE_VOTING_TRUSTEESERVER_H
#define MK_TFHE_VOTING_TRUSTEESERVER_H


#include <trustee/Trustee.h>
#include "stdafx.h"
#include <memory>

namespace server {
	using http_request = web::http::http_request;
	using listener = web::http::experimental::listener::http_listener;

	class TrusteeServer {
		void handle_get(http_request const &message);

		void handle_post(http_request const &message);

		listener m_listener;
		std::shared_ptr<hsr::trustee::Trustee> trustee;
		log4cplus::Logger logger{logger::getDefaultLogger()};
	public:
		TrusteeServer() = default;

		TrusteeServer(utility::string_t const &url, std::shared_ptr<hsr::trustee::Trustee> trustee);

		pplx::task<void> open() { return m_listener.open(); }

		pplx::task<void> close() { return m_listener.close(); }
	};
}

#endif //MK_TFHE_VOTING_TRUSTEESERVER_H
