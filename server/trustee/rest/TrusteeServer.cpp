#include "TrusteeServer.h"
#include "JsonConversion.h"

#include <mk-tfhe-voting/exceptions/CoordinatorError.h>
#include <iostream>
#include <utility>
#include <mkTFHEsamples.h>

namespace server {
	using namespace web::http;

	TrusteeServer::TrusteeServer(utility::string_t const &url, std::shared_ptr<hsr::trustee::Trustee> trustee)
			: m_listener{url},
			  trustee{std::move(trustee)} {
		m_listener.support(methods::GET, std::bind(&TrusteeServer::handle_get, this, std::placeholders::_1));
		m_listener.support(methods::POST, std::bind(&TrusteeServer::handle_post, this, std::placeholders::_1));
	}

	void TrusteeServer::handle_get(http_request const &message) {
		try {
			auto pathString{web::http::uri::decode(message.relative_uri().path())};
			auto paths = web::http::uri::split_path(pathString);
			if (!paths.empty()) {
				// TODO, verify Identity
				if (paths.at(0) == "heartbeat") {
					message.reply(status_codes::OK);
					return;
				} else if (paths.at(0) == "getkeys") {
					web::json::value body{web::json::value::object()};
					auto ks = new_LweKeySwitchKey_array(1, trustee->getParams()->mkParams->n_extract,
														trustee->getParams()->mkParams->dks, trustee->getParams()->mkParams->Bksbit,
														trustee->getParams()->lweParams.get());
					auto keys = trustee->getKeys(ks, this->trustee->getParams()->tLweParams, this->trustee->getParams()->mkParams);
					body["ksk"] = jsonFromKsk(ks);
					body["bsk"] = jsonFromBootstrappingKey(keys.bootstrappingKey);
					body["rLwePubKey"] = jsonFromRlwePub(keys.rLwePubKey);
					message.reply(status_codes::OK, body);
					delete_LweKeySwitchKey_array(1, ks);
					return;
				}
			}
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{"Not implemented GET request "} + pathString));
			message.reply(status_codes::NotFound);
		} catch (std::exception const &e) {
			LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT(std::string{"Unhandled Exception: "} + std::string{e.what()}));
			throw e;
		}
	}

	void TrusteeServer::handle_post(http_request const &message) {
		try {
			auto pathString{web::http::uri::decode(message.relative_uri().path())};
			auto paths = web::http::uri::split_path(pathString);
			if (!paths.empty()) {
				if (paths.at(0) == "sendcounters") {
					auto countersJson = message.extract_json().get();
					message.reply(status_codes::OK);
					try {
						std::pair<unsigned int, std::vector<std::vector<std::shared_ptr<MKLweSample>>>> counters{
								countersFromJson(countersJson, trustee->getParams()->lweParams, trustee->getParams()->mkParams)};
						trustee->sendEncryptedCounters(counters.first, counters.second);
					} catch (web::json::json_exception const &exception) {
						throw CoordinatorError("parsing votes text failed", exception);
					}
					return;
				}
			}
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{"Not implemented POST request "} + pathString));
			message.reply(status_codes::NotFound);
		} catch (std::exception const &e) {
			LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT(std::string{"Unhandled Exception: "} + std::string{e.what()}));
			throw e;
		}
	}
}
