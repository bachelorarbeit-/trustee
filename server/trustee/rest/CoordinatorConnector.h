#ifndef MK_TFHE_VOTING_COORDINATORCONNECTOR_H
#define MK_TFHE_VOTING_COORDINATORCONNECTOR_H


#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <mk-tfhe-voting/helpers/ICoordinatorForTrustee.h>
#include <cpprest/http_client.h>

#include <string>
#include <iostream>
#include <cstdio>

class CoordinatorConnector : public ICoordinatorForTrustee {
	web::http::client::http_client client;
	web::uri self;

	std::string apiKey{};
	unsigned int trusteeNumber{};

	log4cplus::Logger logger{logger::getDefaultLogger()};

	web::http::http_response getRequest(const web::uri &uri);

	web::http::http_response postRequest(const web::uri &uri, const web::json::value &body);

	web::http::http_response tryRequest(web::http::http_request const &httpRequest);

	void checkResponse(const web::http::http_response &httpResponse);

public:
	explicit CoordinatorConnector(web::uri const &coordinatorUri, web::uri selfUri);

	void registerTrusteeToCoordinator() override;

	std::vector<VotesText> getVotes() override;

	std::shared_ptr<TrusteeParams> registerTrusteeToVote(unsigned int voteId) override;

	void publishKeys(std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> pubKey, int32_t n, unsigned voteId) override;

	void publishResults(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<Torus32>> partialDecrypt) override;
};


#endif //MK_TFHE_VOTING_COORDINATORCONNECTOR_H
