#include "rest/TrusteeServer.h"
#include "rest/CoordinatorConnector.h"

#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <trustee/Trustee.h>
#include <mk-tfhe-voting/helpers/SaveEnv.h>

#include <thread>
#include <chrono>

namespace server {

	struct RestServer {
		std::unique_ptr<server::TrusteeServer> trustee;
		std::shared_ptr<CoordinatorConnector> coordCon{};

		log4cplus::Logger logger{logger::getDefaultLogger()};

		void startTrustee(web::uri const &listenUri, web::uri const &externalUri, web::uri const &coordinatorUri) {
			coordCon = std::make_shared<CoordinatorConnector>(coordinatorUri, externalUri);
			auto t = std::make_shared<hsr::trustee::Trustee>(coordCon);
			trustee = std::make_unique<server::TrusteeServer>(listenUri.to_string(), t);
			trustee->open().wait();
			LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT(std::string{"Listening for requests at: "} + listenUri.to_string()));
		}

	public:
		explicit RestServer(utility::string_t const &listenAddress, utility::string_t const &externalAddress) {
			auto coordinatorUri = web::uri_builder{U(getSaveEnv("COORDINATOR_URI", "http://localhost:8080"))}.to_uri();
			web::uri_builder listenUri{listenAddress};
			web::uri_builder externalUri{externalAddress};
			startTrustee(listenUri.to_uri(), externalUri.to_uri(), coordinatorUri);
		}

		void stop() {
			trustee->close().wait();
		}
	};
}

int main() {
	log4cplus::Logger logger{logger::getDefaultLogger()};
	utility::string_t listenAddress = U(std::string{"http://"} + getSaveEnv("SELF_HOSTNAME", "localhost") + ":" + getSaveEnv("SELF_PORT", "42043"));
	utility::string_t externalAddress = U(std::string{"http://"} + getSaveEnv("EXTERNAL_HOSTNAME", "localhost") + ":" + getSaveEnv("EXTERNAL_PORT", "42043"));
	server::RestServer restServer{listenAddress, externalAddress};

	LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Enter quit to terminate"));
	std::string line;
	while (line != "quit") {
		std::this_thread::sleep_for(std::chrono::seconds(5));
		std::getline(std::cin, line);
	}

	restServer.stop();
}
