#ifndef MK_TFHE_VOTING_BULLETINBOARDERROR_H
#define MK_TFHE_VOTING_BULLETINBOARDERROR_H

#include <exceptions/Exception.h>

struct BulletinBoardError : Exception {
	BulletinBoardError(std::string const &message, exception const &cause) : Exception(message, cause) {}

	BulletinBoardError(std::string const &message) : Exception(message) {}
};

#endif //MK_TFHE_VOTING_BULLETINBOARDERROR_H
