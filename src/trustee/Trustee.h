#ifndef TRUSTEE_H_
#define TRUSTEE_H_

#include <mk-tfhe-voting/helpers/ITrusteeForBulletinBoard.h>
#include <mk-tfhe-voting/helpers/ITrusteeForCoordinator.h>
#include <mk-tfhe-voting/helpers/ICoordinatorForTrustee.h>
#include <mk-tfhe-voting/helpers/ITrustee.h>

#include <mk-tfhe-voting/data/DataRepository.h>

#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <tfhe.h>
#include <lweparams.h>
#include <lwekey.h>
#include <tlwe.h>
#include <mkTFHEparams.h>
#include <mkTFHEsamples.h>
#include <lagrangehalfc_arithmetic.h>

#include <memory>

namespace hsr::trustee {
	class Trustee : public ITrusteeForBulletinBoard, public ITrusteeForCoordinator, public ITrustee {
		unsigned int id;
		std::shared_ptr<LweKey> lweKey;
		std::shared_ptr<TLweKey> tlweKey;

		std::weak_ptr<ICoordinatorForTrustee> coordinator;
		std::shared_ptr<TrusteeParams> params;

		std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> publicKeys;
		std::vector<std::shared_ptr<MKTGswUESample>> keyUE{};
		std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey{};

		std::shared_ptr<data::DataRepository> repo;

		log4cplus::Logger logger{logger::getDefaultLogger()};

		std::shared_ptr<TrusteeParams> paramsFromCoordinator();

		std::vector<Torus32> partialDecryptCounter(std::vector<std::shared_ptr<MKLweSample>> const &encryptedCounter) const;

		Torus32 partialDecryptBit(std::shared_ptr<MKLweSample> const &encryptedBit) const;

	public:
		explicit Trustee(std::weak_ptr<ICoordinatorForTrustee> coordinator);

		Torus32 lwePartialDecrypt(const MKLweSample *sample) override;

		void printLweKey();

		void printRlweKey();

		void createLwePubKey(int32_t m, double alpha);

		void createRlwePubKey(std::vector<std::shared_ptr<TorusPolynomial>> pubKey);

		void printRlwePubKey() const;

		std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> getPublicKeys() const;

		const std::vector<std::shared_ptr<MKTGswUESample>> &getKeyUE() const;

		void createUniEncKey(std::vector<std::shared_ptr<TorusPolynomial>> pubKey);

		void sendEncryptedCounters(unsigned int bulletinboardId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters) override;

		KeySwitchingKeys getKeys(LweKeySwitchKey *ks, std::shared_ptr<TLweParams> const &, std::shared_ptr<MKTFHEParams> const &) override;

		std::shared_ptr<TrusteeParams> const &getParams();
	};
}


#endif /* TRUSTEE_H_ */
