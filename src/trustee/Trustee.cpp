#include "Trustee.h"

#include <mk-tfhe-voting/data/model/LwePrivateKey.h>
#include <mk-tfhe-voting/helpers/SaveEnv.h>
#include <mk-tfhe-voting/helpers/Debug.h>

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <stdexcept>
#include <execution>

namespace hsr::trustee {
	Trustee::Trustee(std::weak_ptr<ICoordinatorForTrustee> coordinator) : coordinator{std::move(coordinator)}, params{std::move(paramsFromCoordinator())}, publicKeys{std::make_shared<std::vector<std::shared_ptr<LweSample>>>()} {
		this->id = this->params->id;

		lweKey = std::make_shared<LweKey>(params->lweParams.get());
		tlweKey = std::make_shared<TLweKey>(params->tLweParams.get());
		lweKeyGen(lweKey.get());
		tLweKeyGen(tlweKey.get());

		data::model::LwePrivateKey lwePrivateKey{*lweKey};
		repo = std::make_shared<data::DataRepository>(std::string{"Trustee"} + std::to_string(this->id));
		repo->store(lwePrivateKey);

		createLwePubKey(params->generatedKeys, params->getStdevLwePk());
		createRlwePubKey(params->rlwePubKey);
		createUniEncKey(params->rlwePubKey);

		// TODO adapt key switch gey generation
//		LweKeySwitchKey ks = LweKeySwitchKey(0, 0, 0, nullptr, nullptr);
//		createKeySwitchKey(&ks);

		LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("publicKeys generated"));
		this->coordinator.lock()->publishKeys(publicKeys, params->lwePubKeyParams->n, params->voteId);
		LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("publicKeys published"));
	}


	Torus32 Trustee::lwePartialDecrypt(const MKLweSample *sample) {
		const int32_t n = lweKey->params->n;
		const int32_t offset = params->id * n;
		Torus32 axs = 0;

		for (int i = 0; i < n; i++) {
			axs += sample->a[offset + i] * lweKey->key[i];
		}
		return axs;
	}

	void Trustee::printLweKey() {
		const int32_t n = lweKey->params->n;
		char label[16];

		snprintf(label, sizeof(label), "lwe_%d", params->id);
		std::cout << label << "[" << n << "] ";

		for (int i = 0; i < 32; i++) {
			std::cout << lweKey->key[i];
		}
		std::cout << "...";

		for (int i = n - 32; i < n; i++) {
			std::cout << lweKey->key[i];
		}
		std::cout << std::endl;
	}

	void Trustee::printRlweKey() {
		const int32_t N = tlweKey->params->N;
		char label[16];

		snprintf(label, sizeof(label), "rlwe_%d", params->id);
		std::cout << label << "[" << N << "] ";

		for (int i = 0; i < 32; i++) {
			std::cout << tlweKey->key[0].coefs[i];
		}
		std::cout << "...";

		for (int i = N - 32; i < N; i++) {
			std::cout << tlweKey->key[0].coefs[i];
		}
		std::cout << std::endl;
	}

	void Trustee::createLwePubKey(int32_t m, double alpha) {
		std::vector<data::model::LwePublicKey> lwePubKeys{};
		for (int i = 0; i < m; ++i) {
			std::shared_ptr<LweSample> sample{std::make_shared<LweSample>(this->lweKey->params)};
			lweSymEncrypt(&(*sample), 0, alpha, lweKey.get());
			this->publicKeys->push_back(sample);
			lwePubKeys.emplace_back(data::model::LwePublicKey{*sample, static_cast<unsigned int>(this->lweKey->params->n)});
		}
		this->repo->storeAll(lwePubKeys);
	}

	void Trustee::createRlwePubKey(std::vector<std::shared_ptr<TorusPolynomial>> pubKey) {
		const int32_t N = params->mkParams->N;
		const int32_t dg = params->mkParams->dg;
		const double stdevRLWEkey = params->mkParams->stdevRLWEkey;

		for (int l = 0; l < dg; ++l) {
			rlwePubKey.push_back(std::make_shared<TorusPolynomial>(N));
			for (int i = 0; i < N; ++i) {
				rlwePubKey.at(l)->coefsT[i] = gaussian32(0, stdevRLWEkey);
			}
			torusPolynomialAddMulRFFT1(rlwePubKey.at(l).get(), tlweKey->key, pubKey.at(l).get());
		}
	}

	void Trustee::printRlwePubKey() const {
		std::cout << "rlwePubKey_" << params->id << ":" << std::endl;
		for (unsigned int j = 0; j < this->rlwePubKey.size(); ++j) {
			std::cout << std::setw(3) << j;
			printTorusPolynomial(*rlwePubKey.at(j));
		}
	}

	void Trustee::createUniEncKey(std::vector<std::shared_ptr<TorusPolynomial>> pubKey) {
		const int32_t n = params->mkParams->n;
		const int32_t N = params->mkParams->N;
		const int32_t dg = params->mkParams->dg;
		const double stdevBK = params->mkParams->stdevBK;

		for (int j = 0; j < n; ++j) {
			keyUE.push_back(std::make_shared<MKTGswUESample>(params->tLweParams.get(), params->mkParams.get()));
			keyUE.at(j)->party = params->id;

			// generate r, the randomness
			std::uniform_int_distribution<int32_t> distribution(0, 1);
			IntPolynomial *r = new_IntPolynomial(N);

			for (int i = 0; i < N; ++i) {
				r->coefs[i] = distribution(generator);
			}

			// C = (c0,c1) \in T^2dg, with c0 = s_party*c1 + e_c + generatedKeys*g
			for (int l = 0; l < dg; l++) {
				torusPolynomialUniform(&keyUE.at(j)->c[dg + l]);

				for (int i = 0; i < N; ++i) {
					keyUE.at(j)->c[l].coefsT[i] = gaussian32(0, stdevBK);
				}
				keyUE.at(j)->c[l].coefsT[0] += lweKey->key[j] * params->mkParams->g[l];

				torusPolynomialAddMulR(&keyUE.at(j)->c[l], tlweKey->key, &keyUE.at(j)->c[dg + l]);
			}

			// D = (d0, d1) = r*[Pkey_party | Pkey_parties] + [E0|E1] + [0|generatedKeys*g] \in T^2dg
			for (int l = 0; l < dg; l++) {
				for (int i = 0; i < N; i++) {
					keyUE.at(j)->d[dg + l].coefsT[i] = gaussian32(0, stdevBK);
					keyUE.at(j)->d[l].coefsT[i] = gaussian32(0, stdevBK);
				}
				keyUE.at(j)->d[dg + l].coefsT[0] += lweKey->key[j] * params->mkParams->g[l];

				torusPolynomialAddMulR(&keyUE.at(j)->d[dg + l], r, &(*pubKey.at(l)));
				torusPolynomialAddMulR(&keyUE.at(j)->d[l], r, &(*rlwePubKey.at(l)));
			}

			// F = (f0,f1) \in T^2dg, with f0 = s_party*f1 + e_f + r*g
			for (int l = 0; l < dg; ++l) {
				torusPolynomialUniform(&keyUE.at(j)->f[dg + l]);

				for (int i = 0; i < N; ++i) {
					keyUE.at(j)->f[l].coefsT[i] = gaussian32(0, stdevBK);
					keyUE.at(j)->f[l].coefsT[i] += r->coefs[i] * params->mkParams->g[l];
				}
				torusPolynomialAddMulR(&keyUE.at(j)->f[l], tlweKey->key, &keyUE.at(j)->f[dg + l]);
			}
			delete_IntPolynomial(r);
			keyUE.at(j)->current_variance = stdevBK * stdevBK;
		}
	}


	std::shared_ptr<std::vector<std::shared_ptr<LweSample>>> Trustee::getPublicKeys() const {
		return this->publicKeys;
	}

	const std::vector<std::shared_ptr<MKTGswUESample>> &Trustee::getKeyUE() const {
		return this->keyUE;
	}

	Torus32 Trustee::partialDecryptBit(std::shared_ptr<MKLweSample> const &encryptedBit) const {
		const int32_t n = encryptedBit->n;
		const int32_t offset = id * n;
		Torus32 axs = 0;

		for (int i = 0; i < n; i++) {
			axs += encryptedBit->a[offset + i] * lweKey->key[i];
		}
		return axs;
	}

	std::vector<Torus32> Trustee::partialDecryptCounter(std::vector<std::shared_ptr<MKLweSample>> const &encryptedCounter) const {
		std::vector<Torus32> partialDecryptCounter(encryptedCounter.size());
		std::transform(std::execution::par, encryptedCounter.cbegin(), encryptedCounter.cend(), partialDecryptCounter.begin(), [this](auto &bit) {
			return partialDecryptBit(bit);
		});
		return partialDecryptCounter;
	}

	void Trustee::sendEncryptedCounters(unsigned int bulletinboardId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters) {
		std::vector<std::vector<Torus32>> partialDecrypt(encryptedCounters.size());
		std::transform(std::execution::par, encryptedCounters.cbegin(), encryptedCounters.cend(), partialDecrypt.begin(), [this](auto &counter) {
			return this->partialDecryptCounter(counter);
		});

		this->coordinator.lock()->publishResults(bulletinboardId, this->params->voteId, partialDecrypt);
	}

	std::shared_ptr<TrusteeParams> Trustee::paramsFromCoordinator() {
		coordinator.lock()->registerTrusteeToCoordinator();
		coordinator.lock()->getVotes();

		unsigned voteId{static_cast<unsigned int>(stol(getSaveEnv("VOTE_ID", "1")))};
		return coordinator.lock()->registerTrusteeToVote(voteId);
	}

	KeySwitchingKeys Trustee::getKeys(LweKeySwitchKey *ks, std::shared_ptr<TLweParams> const &, std::shared_ptr<MKTFHEParams> const &) {
		LweKey extractedLweKey{params->extractedLweParams.get()};
		tLweExtractKey(&extractedLweKey, tlweKey.get());
		lweCreateKeySwitchKey(ks, &extractedLweKey, lweKey.get());
		KeySwitchingKeys ksk{};
		ksk.rLwePubKey = rlwePubKey;
		ksk.bootstrappingKey = keyUE;
		return ksk;
	}

	std::shared_ptr<TrusteeParams> const &Trustee::getParams() {
		return params;
	}
}