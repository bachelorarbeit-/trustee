FROM debian:testing
ARG BASE=/opt/trustee
ARG PORT=42043

RUN apt-get -y update
RUN apt-get upgrade --assume-yes
RUN apt-get install --assume-yes cmake git libboost-iostreams-dev libboost-atomic-dev libboost-thread-dev libboost-system-dev libboost-date-time-dev libboost-regex-dev libboost-filesystem-dev libboost-random-dev libboost-chrono-dev libboost-serialization-dev libwebsocketpp-dev openssl libssl-dev ninja-build libcpprest-dev libfftw3-dev build-essential gcc-9 g++-9 cpp-9 libtbb-dev libsqlite3-dev liblog4cplus-dev
RUN mkdir -p ${BASE}/build
COPY src ${BASE}/src
COPY server ${BASE}/server
COPY includes ${BASE}/includes
COPY config ${BASE}/config
COPY config /config
COPY lib ${BASE}/lib
COPY CMakeLists.txt ${BASE}/
RUN cd ${BASE}/build && cmake .. -Dno_tests=on -Dcpprestsdk_DIR=/usr/lib/x86_64-linux-gnu/cmake/
RUN cd ${BASE}/build && cmake --build . -- -j

ENV COORDINATOR_URI=https://coordinator.ba.lag-13.ch/
ENV SELF_HOSTNAME=0.0.0.0
ENV SELF_PORT=${PORT}
ENV EXTERNAL_HOSTNAME=0.0.0.0
ENV EXTERNAL_PORT=${PORT}
ENV VOTE_ID=1

LABEL maintainer="lukas.laetsch@gmail.com"

EXPOSE ${PORT}
WORKDIR /opt/trustee/build/server
ENTRYPOINT ["/opt/trustee/build/server/mk-tfhe-trustee-server"]